
//The below stream filters needs to be deployed in the multichain node and approved by the admin to be
//approvefrom "<admin_address>" "<filter_name>" '{"for":"<stream_name>","approve":true/false}'

//To validate the input key(GTIN format) and reject txns which do not pass
/*create
streamfilter
validateGTIN
{
}
'function filterstreamitem() {
var item = getfilterstreamitem();
for (var k = 0; k < item.keys.length; k++) ;
{
    var key = item.keys;
    var sValidGTIN = "^[0-9]{7}[-][0-9]{6}[-][0-9]{4}$";
    var reValidGTIN = new RegExp(sValidGTIN);
    console.log(reValidGTIN.test(key));
    if (!reValidGTIN.test(key)) {
        return key + " is invalid";
    }
}
}
'*/

/*
create
streamfilter
validateSensorData
{
}
'function filterstreamitem() {
var item = getfilterstreamitem();
var value = item.data.text;
console.log(value);
if (value > 10) {
    return value + " value is above the allowed limit;
}
;
}
'*/